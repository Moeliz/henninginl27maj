# Continuous Deployment av en Java EE-webbtjänst med gitlab-ci
Henning Urrutia-Målquist

Yrgo Java EE-utvecklare

Göteborg 2018-05-27
### Sammanfattning

Jag har skapat en Java EE-Webbtjänst som skriver till och läser från en *postgres*-databas. För detta används *Docker*, *Docker-compose*  och *NGINX* används som en proxy-server. Vid varje commit byggs projektet på gitlab och en image publiceras på GitLab:s *Container Registry*. Denna image publiceras därefter automatiskt på en server med hjälp av en *ssh*-signal som triggar igång ett script på servern. För att undvika nedtid används blue/green deployment.

Applikation kan köras med:
`curl http://96.126.97.13/api/?name=Jon`
`curl --data 'bye=Jon' --request POST http://96.126.97.13/api/`


### Rapport
##### Bygga projektet
För att bygga projektet används Maven. För detta behövs en `pom.xml`-fil och där specificeras att det är en war-fil som ska byggas och även  beroenden(dependencies). I detta fall specificeras att det är ett Java EE projekt samt att databasen postgres ska användas. Utöver detta specificeras plugins för maven och för att maven ska kunna bygga en war-fil. 

##### Konfigurera databasen
För att koppla samman detta med databasen postgresql behöver projektet byggas med en `percistance.xml`-fil där data-source sätts. I detta fall:
```xml
<jta-data-source>java:/PostgresDS</jta-data-source>
```

WildFly behöver veta detta för att göra kopplingen mellan applikationen och databasen. Wildfly behöver också en `postgressql.jar`-fil och en `module.xml`-fil.
Dessa, samt java-applikationens war-fil konfigureras i en Docker-image genom en Dockerfile.
```bash
FROM jboss/wildfly:11.0.0.Final

COPY ./postgresql/postgresql-42.2.2.jar $JBOSS_HOME/modules/system/layers/base/org/postgresql/main/
COPY ./postgresql/module.xml $JBOSS_HOME/modules/system/layers/base/org/postgresql/main/

RUN sh -c "$JBOSS_HOME/bin/standalone.sh &" && \
    sleep 15 && \
    $JBOSS_HOME/bin/jboss-cli.sh --connect --command="/subsystem=datasources/jdbc-driver=postgresql-driver:add(driver-class-name=org.postgresql.Driver, driver-module-name=org.postgresql, driver-name=postgresql-driver" && \
    $JBOSS_HOME/bin/jboss-cli.sh --connect --command="data-source add --connection-url=jdbc:postgresql://postgres/ --driver-name=postgresql-driver --name=PostgresDS --jndi-name=java:/PostgresDS --user-name=postgres" && \
    $JBOSS_HOME/bin/jboss-cli.sh --connect --command=:shutdown

RUN rm -R /opt/jboss/wildfly/standalone/configuration/standalone_xml_history/current/*

COPY ./target/webapp-0.0.1-SNAPSHOT.war $JBOSS_HOME/standalone/deployments/

```

### Continuous Integration med GitLab

Istället för att bygga docker-imagen lokalt används här gitlabs verktyg för contiuous Integration, GitLab CI. För detta skapas en `.gitlab-ci.yml`-fil. I denna fil konfigureras vilka ci-operationer som ska göra varje commit. Dessa operationer ingår då i en *pipeline* som i sin tur har *stages* som körs sekventiellt. Varje stage kan innehålla ett eller flera *jobs* som kan köras parallellt.  I detta fall tre stages som var och en har ett job:

```yml
stages:
  - build-war
  - build-image
  - stage-deployment
```
War-filen byggs likt beskrivet ovan på gitlab. För att gitlab-ci ska bygga med maven används en maven-image från Docker Hub som bygger en container. I containern är maven installerat och då kan ett script köras för att säga åt maven att bygga war-filen. Filer som ska behållas specificeras i *artifacts*.

```yml
build-war:
  stage: build-war
  image: maven:3-jdk-8
  script:
    - mvn -B package
  artifacts:
    paths:
      - target/webapp-0.0.1-SNAPSHOT.war
```
I nästa stage byggs en image. Denna image publiceras sedan till *GitLabs CI*-tjänst *Container Registry*.

```yml
build-image:
  image: docker:stable
  services:
  - docker:dind
  stage: build-image
  script:
    - docker login -p $CI_JOB_TOKEN registry.gitlab.com -u gitlab-ci-token
    - docker build -t registry.gitlab.com/moeliz/henninginl27maj/webapp .
    - docker push registry.gitlab.com/moeliz/henninginl27maj/webapp
```
Denna image kan sedan laddas ner till docker såhär:

```docker pull registry.gitlab.com/moeliz/henninginl27maj/webapp```


##### Continuous deployment

Det sista stage:t körs för att automatisera lansering av den senaste versionen av applikationen på servern.

```yml
stage-deployment:
  stage: stage-deployment
  script:
    # CI_BUILD_REF references the branch rather than the commit
    # ID, so we are grabbing the commit ID from Git outselves
    - COMMIT_ID=$(git rev-parse HEAD)
    - TAG="$COMMIT_ID"
    - bash trigger-deployment.sh $TAG
```
Här körs ett script `trigger-deployment.sh`som skickar en ssh-signal till servern som i sin tur startar ett script för att starta lanseringen.
För att detta ska fungera krävs en del konfiguration:

###### SSH på servern
* En ny användare vid namn `deploy` skapas
```bash
$ adduser deploy
```

* En ssh-nyckel skapas med `ssh-keygen` och den publika ska till `deploy` och den privata till gitlab.
* Ett script skapas som laddar ner den senaste image som skapades i `build-image`-stage:t och sätter en tag på den senaste versionen samt ett script för att byta till den nya versionen.

```bash
#!/bin/bash
COLOR=$(sh get_next_color.sh)
set -e

fail () {
	    error="$1"
	        [[ -n $error ]] || error="Deployment failed"
		    echo "Error: $error"
		        exit 1
		}

		read TAG

		[[ -n $TAG ]] || fail "No tag specified"

		export TAG

		echo "Using tag: $TAG"

docker pull registry.gitlab.com/moeliz/henninginl27maj/webapp
docker tag registry.gitlab.com/moeliz/henninginl27maj/webapp webapp-$COLOR
sh deploy1.sh
```
* Scriptet görs körbart
```bash
chmod u+x deploy.sh
```

* `root` ger `deploy` tillåtelse att endast köra detta script.
```bash
chsh --shell /home/deploy/deploy.sh deploy
```

##### GitLab SSH

GitLab behöver följande variabler för att kunna skicka rätt ssh-signal till rätt server:

* STAGING_SSH_REMOTE_KEY
* SSH_PRIVATE_KEY
* STAGING_SERVER

### Docker-compose

Docker-compose är konfigurerat med fyra services. Den drar alltså igång fyra containers. En proxy-container byggd utifrån en NGINX-image(där konfigurationsfilerna skickas in till containern via Volumes), en databas-container byggd utifrån en postgres-image samt två webapp-containers som är byggda utifrån imagen skapad på gitlab.
```yml
version: '3'
services:
  postgres:
    image: postgres
  webapp-blue:
    ports:
      - "8080"
    image: webapp-blue
  webapp-green:
    ports:
      - "8080"
    image: webapp-green
  proxy:
    image: nginx
    ports:
      - "80:80"
    restart: unless-stopped
    volumes:
      - ./nginx.conf:/etc/nginx/nginx.conf
      - ./webapp.conf:/etc/nginx/upstreams/webapp.conf

```

### Blue/green deployment
För att kunna lansera nya uppdateringar ofta och utan nedtid används här blue/green deployment. Alltså att när en ny version lanseras styrs trafiken över till den.
För att skilja på de olika versionerna av *webapp* åt sätts en tag varje gång en ny version laddas ner, blue eller green. För att hålla koll på vilken version som är igång används Consul.

Consul är en key/value store som lagrar vilken version som körs
```bash
curl --data $COLOR --request PUT http://127.0.0.1:8500/v1/kv/webapp/color
```
För att kolla vilken färg som är satt kan följande skrivas
```bash
curl --silent http://127.0.0.1:8500/v1/kv/webapp/color?raw
```

För att få ut nästa färg körs scriptet `get_next_color.sh`


Registrator har här använts för att populera consul med vilka services(tjänster, t.ex. nginx) som är igång. Registrator lyssnar på docker och har koll på om en container startar eller stoppas.
Detta kan åstakommas med följande:

```bash
docker run --detach --name registrator --net=host --rm --volume /var/run/docker.sock:/tmp/docker.sock gliderlabs/registrator consul://
```
Då kan vi se vilka tjänster som är igång med följande:
```bash
root@localhost:/home/deploy# curl http://127.0.0.1:8500/v1/catalog/services?pretty
{
    "consul": [],
    "nginx": [],
    "webapp-blue": [],
    "webapp-green": []
}
```

#### NGINX

Nginx lyssnar på port 80 och skickar vidare till `http://webapp/webapp-0.0.1-SNAPSHOT/` där applikationen nås. I upstreams hittas `webapp.conf` med IP och port till webbtjänsten som anropet vidarebefordras till.
```
events {
}

http {
    log_format format 'Calling upstream on $upstream_addr';

    server {
        access_log /var/log/nginx/access.log format;

        listen 80;

        location /api {
            proxy_pass http://webapp/webapp-0.0.1-SNAPSHOT/;
        }
    }

    include upstreams/*.conf;
}
```
Hur `webapp.conf` ska vara konfigurerad beror på vilken tjänst som ska köras. I detta fall ska filen vara konfigurerad så att anrop görs mot den senaste versionen av tjänsten. Detta görs här med Consul Template.
Två mallar behövs. En för varje tjänst som ska anropas:

```
upstream webapp {
{{range service "webapp-blue"}}    server {{.Address}}:8080;
{{end}}}
```
Motsvarande görs för webapp-green.

Denna mall kan sedan användas för att updatera  `webapp.conf`-fil och alltså rikta om trafiken till den andra versionen.

```bash
consul-template -once -template "webapp-$COLOR.ctmpl:webapp.conf.tmp" && \
cat webapp.conf.tmp > webapp.conf
```

För att dra igång den senaste versionen av tjänsten körs scriptet `deploy1.sh` som först kollar vilken nästa färg är och stoppar och tar bort den tidigare versionen av denna. Sedan startas den upp på nytt(den är redan nedladdad och taggad i scriptet `deplpy.sh`).
För att wildfly ska hinna starta ordentligt i containern körs ett `sleep`-kommando som gör att scriptet avvaktar i 40 sekunder innan det kör vidare.
Därefter updateras `webapp.conf`  till den senaste tjänsten samt instruerar NGINX att läsa om webapp.conf.
Till sist updaterar vi vilken färg som consul ska spara i sin key/value store.


```bash
#!/bin/sh
COLOR=$(sh get_next_color.sh)
docker-compose stop webapp-$COLOR
docker-compose rm -f webapp-$COLOR
docker-compose up -d webapp-$COLOR &&
      sleep 40 && \
    	consul-template -once -template "webapp-$COLOR.ctmpl:webapp.conf.tmp" && \
      cat webapp.conf.tmp > webapp.conf && \
    	rm webapp.conf.tmp && \
      docker-compose kill -s HUP proxy && \
    	curl --data $COLOR --request PUT http://127.0.0.1:8500/v1/kv/webapp/color
```

### DevOps

DevOps som är en förkortning av Development och Operations, är en kultur och ett arbetsätt inom mjukvaruutveckling. DevOps siktar på att få ut nya pålitliga lanseringar snabbt. Detta kan uppnås genom överskådlig automatisering av kedjan mellan utveckling och lansering.
Denna kedja består av
* Code
* Build
* Test
* Package
* Release
* Configure
* Monitor

##### DevOps koppling till detta projekt
Java EE-projektet är konfigurerat på så sätt att vid varje commit byggs projektet automatiskt med Contiuous Integration samt att det automatiskt lanseras en ny version av projektet på servern. En reversed-proxy är konfigurerad på servern vilket flyttar komplexitet från utvecklingslagret till operations-lagret vilket har underlättat automatiseringen.

 Vid vidareutveckling av projektet behövs ingen mer konfiguration på servern utan allt jobb sker lokalt. Automatisk testning, som är en del av DevOps, skulle också kunna göras via GitLab Ci.  

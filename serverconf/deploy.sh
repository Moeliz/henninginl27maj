#!/bin/bash
COLOR=$(sh get_next_color.sh)
set -e

fail () {
	    error="$1"
	        [[ -n $error ]] || error="Deployment failed"
		    echo "Error: $error"
		        exit 1
		}

		read TAG

		[[ -n $TAG ]] || fail "No tag specified"

		export TAG

		echo "Using tag: $TAG"

docker pull registry.gitlab.com/moeliz/henninginl27maj/webapp
docker tag registry.gitlab.com/moeliz/henninginl27maj/webapp webapp-$COLOR
sh deploy1.sh

#!/bin/sh
COLOR=$(sh get_next_color.sh)
docker-compose stop webapp-$COLOR
docker-compose rm -f webapp-$COLOR
docker-compose up -d webapp-$COLOR &&
      sleep 40 && \
    	consul-template -once -template "webapp-$COLOR.ctmpl:webapp.conf.tmp" && \
      cat webapp.conf.tmp > webapp.conf && \
    	rm webapp.conf.tmp && \
      docker-compose kill -s HUP proxy && \
    	curl --data $COLOR --request PUT http://127.0.0.1:8500/v1/kv/webapp/color

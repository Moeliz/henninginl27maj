import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;
import javax.persistence.Query;


@Stateless
public class GreetService {

    @PersistenceContext(unitName = "webapp")
    private EntityManager entityManager;

    public String greet(String greet) {
        Name name = new Name();
        name.setName(greet);
        entityManager.persist(name);
        return "Hej " + greet + "!";

    }
    public List<Name> findAll() {
      Query q = entityManager.createQuery("select * from Name name");
      List<Name> names = q.getResultList();
      return names;
    }

}

import java.io.IOException;

import javax.inject.Inject;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;


@WebServlet("/")
public class GreetServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Inject
    private GreetService greetService;


    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String name;
        List<Name> message;

        response.setContentType("text/plain");

        // all = request.getParameter("all");

        message = greetService.findAll();

        response.getWriter().println(message);
    }
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String name;
        String message;

        response.setContentType("text/plain");

        name = request.getParameter("name");

        message = greetService.greet(name);

        response.getWriter().println(message);
    }
}
